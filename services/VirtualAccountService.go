package services

import "go.mongodb.org/mongo-driver/mongo"

type VirtualAccountService struct {
	BaseService
}

func CreateVirtualAccountService(mongoClient *mongo.Client) VirtualAccountService {
	virtualAccountService := VirtualAccountService{}
	virtualAccountService.MongoClient = mongoClient
	return virtualAccountService
}