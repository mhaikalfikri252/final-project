package apis

import (
	"context"
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/datanosql"
	"net/http"
)

type TopUpApi struct {
	BaseApi
	datanosql.TopUp

	TopUpID string `json:"topUpID"`
}

func (topUpApi TopUpApi) GetTopUps(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = topUpApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	topUpModel := datanosql.CreateTopUp(mongoClient)
	topUps, err := topUpModel.ListTopUps(ctx)
	if err != nil {
		topUpApi.Error(w, err)
		return
	} else {
		topUpApi.Json(w, topUps, http.StatusOK)
		return
	}
}

func (topUpApi TopUpApi) GetTopUpByID(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = topUpApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	topUpID := topUpApi.QueryParam(r, "topUpID")

	topUpModel := datanosql.CreateTopUp(mongoClient)
	err = topUpModel.FindOne(ctx, topUpID)
	if err != nil {
		topUpApi.Error(w, err)
		return
	} else {
		topUpApi.Json(w, topUpModel, http.StatusOK)
		return
	}

}

func (topUpApi TopUpApi) PostTopUp(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&topUpApi)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = topUpApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	topUpModel := datanosql.CreateTopUp(mongoClient)
	topUp, err := topUpModel.AddTopUp(ctx,
		topUpApi.BankCode, topUpApi.BankName,
		topUpApi.VaAccountNo, topUpApi.VaAccountName,
		topUpApi.TopUpAmount)

	if err != nil {
		topUpApi.Error(w, err)
		return
	} else {
		topUpApi.Json(w, topUp, http.StatusOK)
		return
	}
}

func (topUpApi TopUpApi) UpdateTopUp(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&topUpApi)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = topUpApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	topUpModel := datanosql.CreateTopUp(mongoClient)
	err = topUpModel.Update(ctx, topUpApi.TopUpID,
		topUpApi.BankCode, topUpApi.BankName,
		topUpApi.VaAccountNo, topUpApi.VaAccountName,
		topUpApi.TopUpAmount)

	if err != nil {
		topUpApi.Error(w, err)
		return
	} else {
		topUpApi.Json(w, topUpModel, http.StatusOK)
		return
	}

}

func (topUpApi TopUpApi) RemoveTopUp(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&topUpApi)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = topUpApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		topUpApi.Error(w, err)
		return
	}

	topUpModel := datanosql.CreateTopUp(mongoClient)
	err = topUpModel.Delete(ctx, topUpApi.TopUpID)
	if err != nil {
		topUpApi.Error(w, err)
		return
	} else {
		topUpApi.Json(w, topUpModel, http.StatusOK)
		return
	}
}
