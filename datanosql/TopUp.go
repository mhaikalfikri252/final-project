package datanosql

import (
	"context"
	"errors"
	"final-project/config"
	"log"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type TopUp struct {
	BaseNoSql `bson:"-"`

	TopUpID       string    `json:"topUpID" bson:"_topUpID"`
	BankCode      string    `json:"bankCode" bson:"bankCode"`
	BankName      string    `json:"bankName" bson:"bankName"`
	VaAccountNo   string    `json:"vaAccountNo" bson:"vaAccountNo"`
	VaAccountName string    `json:"vaAccountName" bson:"vaAccountName"`
	TopUpAmount   float64   `json:"topUpAmount" bson:"topUpAmount"`
	CreatedAt     time.Time `json:"createdAt" bson:"createdAt"`
}

func CreateTopUp(client *mongo.Client) TopUp {
	topUp := TopUp{}
	topUp.Client = client
	return topUp
}

func (topUp TopUp) Collection() *mongo.Collection {
	session, _ := topUp.Client.StartSession()
	session.StartTransaction()

	return topUp.Client.Database(config.DATABASE).Collection("topups")
}

func (topUp TopUp) Truncate(ctx context.Context) error {
	return topUp.Collection().Drop(ctx)
}

func (topUp TopUp) AddTopUp(ctx context.Context,
	bankCode string, bankName string,
	vaAccountNo string, vaAccountName string,
	topUpAmount float64) (*TopUp, error) {

	topUp.TopUpID = uuid.New().String()
	topUp.BankCode = bankCode
	topUp.BankName = bankName
	topUp.VaAccountNo = vaAccountNo
	topUp.VaAccountName = vaAccountName
	topUp.TopUpAmount = topUpAmount
	topUp.CreatedAt = time.Now().UTC()

	_, err := topUp.Collection().InsertOne(ctx, topUp)
	if err != nil {
		return nil, err
	}
	return &topUp, nil

}

func (topUp *TopUp) FindOne(ctx context.Context, topUpID string) error {


	filter := bson.D{
		primitive.E{Key: "_topUpID", Value: topUpID},
	}

	result := topUp.Collection().FindOne(ctx, filter)

	if result.Err() != nil {

		log.Println(result.Err())
		if result.Err().Error() == config.MONGO_NO_DOCUMENT {
			return errors.New("top_up_not_found")
		}
		return result.Err()
	}

	err := result.Decode(&topUp)
	if err != nil {
		return err
	}

	return nil

}

func (topUp *TopUp) Update(ctx context.Context,
	topUpID string,
	bankCode string, bankName string,
	vaAccountNo string, vaAccountName string,
	topUpAmount float64) error {


	filter := bson.D{
		primitive.E{Key: "_topUpID", Value: topUpID},
	}

	set := bson.D{
		primitive.E{
			Key: "$set",
			Value: bson.D{
				primitive.E{Key: "bankCode", Value: bankCode},
				primitive.E{Key: "bankName", Value: bankName},
				primitive.E{Key: "vaAccountNo", Value: vaAccountNo},
				primitive.E{Key: "vaAccountName", Value: vaAccountName},
				primitive.E{Key: "topUpAmount", Value: topUpAmount},
			},
		},
	}

	optionsAfter := options.After
	updateOptions := &options.FindOneAndUpdateOptions{
		ReturnDocument: &optionsAfter,
	}

	result := topUp.Collection().FindOneAndUpdate(ctx, filter, set, updateOptions)
	if result.Err() != nil {
		return result.Err()
	}

	return result.Decode(&topUp)

}

func (topUp TopUp) Delete(ctx context.Context, topUpID string) error {


	filter := bson.D{
		primitive.E{Key: "_topUpID", Value: topUpID},
	}

	result, err := topUp.Collection().DeleteOne(ctx, filter, nil)
	if err != nil {
		return err
	}
	log.Println(result)
	return nil

}

func (topUp TopUp) ListTopUps(ctx context.Context) ([]TopUp, error) {

	topups := []TopUp{}
	filter := bson.D{}
	cursor, err := topUp.Collection().Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(ctx, &topups)
	if err != nil {
		return nil, err
	}

	return topups, nil

}
