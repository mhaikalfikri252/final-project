package datasql

import "database/sql"

type BaseDataSql struct {
	DB             *sql.DB `json:",omitempty"`
	Transaction    *sql.Tx `json:",omitempty"`
	UseTransaction bool    `json:",omitempty"`
}

func (baseDataSql BaseDataSql) Exec(sql string, args ...interface{}) (sql.Result, error) {
	if baseDataSql.UseTransaction {
		return baseDataSql.Transaction.Exec(sql, args...)
	} else {
		return baseDataSql.DB.Exec(sql, args...)
	}
}

func (baseDataSql BaseDataSql) QueryRow(sql string, args ...interface{}) *sql.Row {
	if baseDataSql.UseTransaction {
		return baseDataSql.Transaction.QueryRow(sql, args...)
	} else {
		return baseDataSql.DB.QueryRow(sql, args...)
	}
}

func (baseDataSql BaseDataSql) Query(sql string, args ...interface{}) (*sql.Rows, error) {
	if baseDataSql.UseTransaction {
		return baseDataSql.Transaction.Query(sql, args...)
	} else {
		return baseDataSql.DB.Query(sql, args...)
	}
}
