package helpers

func Empty(val string) bool {
	if val == "" {
		return true
	} else {
		return false
	}
}

//greater than zero
func Gtz(val int) bool {
	if val > 0 {
		return true
	} else {
		return false
	}
}

//greater than or equals to
func Float64Gtez(val float64) bool {
	if val >= 0 {
		return true
	} else {
		return false
	}
}
func Float64Gtz(val float64) bool {
	if val > 0 {
		return true
	} else {
		return false
	}
}

func InString(val string, arr []string) bool {
	for i := 0; i < len(arr); i++ {
		if arr[i] == val {
			return true
		}
	}
	return false
}
