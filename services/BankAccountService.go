package services

import "go.mongodb.org/mongo-driver/mongo"

type BankAccountService struct {
	BaseService
}

func CreateBankAccountService(mongoClient *mongo.Client) BankAccountService {
	bankAccountService := BankAccountService{}
	bankAccountService.MongoClient = mongoClient
	return bankAccountService
}