package datanosql

import (
	"context"
	"errors"
	"log"
	"time"
	"final-project/config"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	TRANSACTION_TYPE_TOPUP  = "Topup"
	TRANSACTION_TYPE_TRANSFER = "Transfer"
	TRANSACTION_TYPE_PAY = "Pay"
)

var TRANSACTION_TYPES []string = []string{
	TRANSACTION_TYPE_TOPUP,
	TRANSACTION_TYPE_TRANSFER,
	TRANSACTION_TYPE_PAY,
}

type VaTransaction struct {
	BaseNoSql `bson:"-"`

	HistoryID         primitive.ObjectID `json:"historyID" bson:"_historyID"`
	VaAccountNo       string             `json:"vaAccountNo" bson:"vaAccountNo"`
	VaAccountName     string             `json:"vaAccountName" bson:"vaAccountName"`
	TransactionAmount float64            `json:"transactionAmount" bson:"transactionAmount"`
	TransactionType   string             `json:"transactionType" bson:"transactionType"`
	Description       string             `json:"description" bson:"description"`
	TransactionID     string             `json:"transactionID" bson:"transactionID"`
	CreatedAt         time.Time          `json:"createdAt" bson:"createdAt"`
}

func CreateVaTransaction(client *mongo.Client) VaTransaction {
	vaTransaction := VaTransaction{}
	vaTransaction.Client = client
	return vaTransaction
}

func (vaTransaction VaTransaction) Collection() *mongo.Collection {
	session, _ := vaTransaction.Client.StartSession()
	session.StartTransaction()

	return vaTransaction.Client.Database(config.DATABASE).Collection("vatransactions")
}

func (vaTransaction VaTransaction) Truncate(ctx context.Context) error {
	return vaTransaction.Collection().Drop(ctx)
}

func (vaTransaction VaTransaction) AddVaTransaction(ctx context.Context,
	vaAccountNo string, vaAccountName string,
	transactionAmount float64, transactionType string,
	description string, transactionID string) (*VaTransaction, error) {

	vaTransaction.HistoryID = primitive.NewObjectID()
	vaTransaction.VaAccountNo = vaAccountNo
	vaTransaction.VaAccountName = vaAccountName
	vaTransaction.TransactionAmount = transactionAmount
	vaTransaction.TransactionType = transactionType
	vaTransaction.Description = description
	vaTransaction.TransactionID = transactionID
	vaTransaction.CreatedAt = time.Now().UTC()

	_, err := vaTransaction.Collection().InsertOne(ctx, vaTransaction)
	if err != nil {
		return nil, err
	}
	return &vaTransaction, nil

}

func (vaTransaction *VaTransaction) FindOne(ctx context.Context, historyID string) error {

	oid, err := primitive.ObjectIDFromHex(historyID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_historyID", Value: oid},
	}

	result := vaTransaction.Collection().FindOne(ctx, filter)

	if result.Err() != nil {

		log.Println(result.Err())
		if result.Err().Error() == config.MONGO_NO_DOCUMENT {
			return errors.New("virtual_account_transaction_not_found")
		}
		return result.Err()
	}

	err = result.Decode(&vaTransaction)
	if err != nil {
		return err
	}

	return nil

}

func (vaTransaction *VaTransaction) Update(ctx context.Context,
	historyID string,
	vaAccountNo string, vaAccountName string,
	transactionAmount float64, transactionType string,
	description string, transactionID string) error {

	oid, err := primitive.ObjectIDFromHex(historyID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_historyID", Value: oid},
	}

	set := bson.D{
		primitive.E{
			Key: "$set",
			Value: bson.D{
				primitive.E{Key: "vaAccountNo", Value: vaAccountNo},
				primitive.E{Key: "vaAccountName", Value: vaAccountName},
				primitive.E{Key: "transactionAmount", Value: transactionAmount},
				primitive.E{Key: "transactionType", Value: transactionType},
				primitive.E{Key: "description", Value: description},
				primitive.E{Key: "transactionID", Value: transactionID},
			},
		},
	}

	optionsAfter := options.After
	updateOptions := &options.FindOneAndUpdateOptions{
		ReturnDocument: &optionsAfter,
	}

	result := vaTransaction.Collection().FindOneAndUpdate(ctx, filter, set, updateOptions)
	if result.Err() != nil {
		return result.Err()
	}

	return result.Decode(&vaTransaction)

}

func (vaTransaction VaTransaction) Delete(ctx context.Context, historyID string) error {

	oid, err := primitive.ObjectIDFromHex(historyID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_historyID", Value: oid},
	}

	result, err := vaTransaction.Collection().DeleteOne(ctx, filter, nil)
	if err != nil {
		return err
	}
	log.Println(result)
	return nil

}

func (vaTransaction VaTransaction) ListVaTransactions(ctx context.Context) ([]VaTransaction, error) {

	vaTransactions := []VaTransaction{}
	filter := bson.D{}
	cursor, err := vaTransaction.Collection().Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(ctx, &vaTransactions)
	if err != nil {
		return nil, err
	}

	return vaTransactions, nil

}
