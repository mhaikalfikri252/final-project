package datasql

import (
	"log"
	"testing"
	"final-project/config"
	"final-project/connection"
	"final-project/helpers"
)

func TestVirtualAccount_Add(t *testing.T) {

	db := connection.OpenConnection(config.CURRENT_PHASE)
	defer db.Close()

	virtualAccountModel := CreateVirtualAccount(db)

	err := virtualAccountModel.Truncate()
	if err != nil {
		t.Fatal(err.Error())
	}

	accountNo := "BA-001"
	noHp := "081289899898"
	email := "wakwaw@gmail.com"
	accountName := "Wakwaw"
	pin := "123456"
	password := "wakwak123"
	saldo := float64(10000000)
	seqNo := "02134"

	err = virtualAccountModel.Add(accountNo, noHp, email, accountName, pin, password, saldo, seqNo)
	if err != nil {
		t.Fatal(err.Error())
	}

	virtualAccounts, err := virtualAccountModel.GetVirtualAccounts()
	if err != nil {
		t.Fatal(err)
	}
	log.Println("OnInserted", helpers.ToJson(virtualAccounts))

	if len(virtualAccounts) != 1 {
		t.Fatal("Expected=1, actual=", len(virtualAccounts))
	}

	// update
	// accountNo = "BA-002"
	// noHp = "0812-0808-1212"
	// email = "random@gmail.com"
	// accountName = "random"
	// pin = "654321"
	// password = "random123"
	// saldo = float64(5000000)
	// seqNo = "09876"

	// err = virtualAccountModel.Update(accountNo, noHp, email, accountName, pin, password, saldo, seqNo)
	// if err != nil {
	// 	t.Fatal(err.Error())
	// }

	// foundVirtualAccount, err := virtualAccountModel.FindOne(accountNo)
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// if foundVirtualAccount == nil {
	// 	t.Fatal("FindOne=VirtualAccount=", foundVirtualAccount)
	// }

	// if foundVirtualAccount.AccountNo != accountNo {
	// 	t.Fatal("Expected=", accountNo, "actual=", foundVirtualAccount.AccountNo)
	// }
	// if foundVirtualAccount.NoHp != noHp {
	// 	t.Fatal("Expected=", noHp, "actual=", foundVirtualAccount.NoHp)
	// }
	// if foundVirtualAccount.Email != email {
	// 	t.Fatal("Expected=", email, "actual=", foundVirtualAccount.Email)
	// }
	// if foundVirtualAccount.AccountName != accountName {
	// 	t.Fatal("Expected=", accountName, "actual=", foundVirtualAccount.AccountName)
	// }
	// if foundVirtualAccount.Pin != pin {
	// 	t.Fatal("Expected=", pin, "actual=", foundVirtualAccount.NoHp)
	// }
	// if foundVirtualAccount.Password != password {
	// 	t.Fatal("Expected=", password, "actual=", foundVirtualAccount.Password)
	// }
	// if foundVirtualAccount.Saldo != saldo {
	// 	t.Fatal("Expected=", saldo, "actual=", foundVirtualAccount.Saldo)
	// }
	// if foundVirtualAccount.SeqNo != seqNo {
	// 	t.Fatal("Expected=", seqNo, "actual=", foundVirtualAccount.SeqNo)
	// }

	virtualAccounts, err = virtualAccountModel.GetVirtualAccounts()
	if err != nil {
		t.Fatal(err)
	}
	log.Println("OnUpdated", helpers.ToJson(virtualAccounts))

	// Remove
	// err = virtualAccountModel.Remove(accountNo)
	// if err != nil {
	// 	t.Fatal(err.Error())
	// }

	// virtualAccounts, err = virtualAccountModel.GetVirtualAccounts()
	// if err != nil {
	// 	t.Fatal(err)
	// }

	// if len(virtualAccounts) != 0 {
	// 	t.Fatal("Expected=0, actual=", len(virtualAccounts))
	// }

}
