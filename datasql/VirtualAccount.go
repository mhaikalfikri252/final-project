package datasql

import (
	"database/sql"
	"encoding/base64"
	"errors"
	"final-project/helpers"
	"log"
	"time"

	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

type VirtualAccount struct {
	BaseDataSql

	AccountNo   string  `json:"accountNo"`
	NoHp        string  `json:"noHp"`
	Email       string  `json:"email"`
	AccountName string  `json:"accountName"`
	Pin         string  `json:"pin"`
	Password    string  `json:"password"`
	Saldo       float64 `json:"saldo"`
	SeqNo       string  `json:"seqNo"`

	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
}

func CreateVirtualAccount(db *sql.DB) VirtualAccount {
	virtualAccount := VirtualAccount{}
	virtualAccount.DB = db
	return virtualAccount
}

func CreateVirtualAccountWithTransaction(transaction *sql.Tx) VirtualAccount {
	virtualAccount := VirtualAccount{}
	virtualAccount.Transaction = transaction
	virtualAccount.UseTransaction = true
	return virtualAccount
}

func (virtualAccount VirtualAccount) Add(
	accountNo string, noHp string, email string,
	accountName string, pin string, password string,
	saldo float64, seqNo string) error {

	if helpers.Empty(accountNo) {
		return errors.New("invalid_account_no")
	}

	if helpers.Empty(noHp) {
		return errors.New("invalid_no_hp")
	}

	if helpers.Empty(email) {
		return errors.New("invalid_email")
	}

	if helpers.Empty(accountName) {
		return errors.New("invalid_account_name")
	}

	if !helpers.Float64Gtz(saldo) {
		return errors.New("invalid_saldo")
	}

	if helpers.Empty(seqNo) {
		return errors.New("invalid_seq_no")
	}

	sql := `insert into virtual_accounts (
		account_no, no_hp, email, account_name,
		pin, password, saldo, seq_no,
		created_at) values ($1, $2, $3 ,$4, $5, $6, $7, $8, $9)`

	enPassw, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	enPin := base64.StdEncoding.EncodeToString([]byte(pin))

	result, err := virtualAccount.Exec(sql,
		accountNo, noHp, email,
		accountName, enPin, enPassw,
		saldo, seqNo, time.Now().UTC())

	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("VirtualAccount.Add.AffectedRows=", affectedRows)
	return nil

}

func (virtualAccount VirtualAccount) fetchRow(cursor *sql.Rows) (VirtualAccount, error) {
	b := VirtualAccount{}
	err := cursor.Scan(
		&b.AccountNo, &b.NoHp, &b.Email,
		&b.AccountName, &b.Pin, &b.Password,
		&b.Saldo, &b.SeqNo, &b.CreatedAt,
		&b.UpdatedAt)
	if err != nil {
		return VirtualAccount{}, err
	}
	return b, nil
}

func (virtualAccount VirtualAccount) selectQuery() string {
	sql := `select 
	account_no, no_hp, email, 
	account_name, pin, password,
	saldo, seq_no, created_at, 
	updated_at from virtual_accounts`
	return sql
}

func (virtualAccount VirtualAccount) GetVirtualAccounts() ([]VirtualAccount, error) {

	sql := virtualAccount.selectQuery()
	cursor, err := virtualAccount.Query(sql)
	if err != nil {
		return nil, err
	}

	virtualAccounts := []VirtualAccount{}

	for cursor.Next() {
		b, err := virtualAccount.fetchRow(cursor)
		if err != nil {
			return nil, err
		}
		virtualAccounts = append(virtualAccounts, b)
	}

	return virtualAccounts, nil
}

func (virtualAccount VirtualAccount) FindOne(accountNo string) (*VirtualAccount, error) {

	if helpers.Empty(accountNo) {
		return nil, errors.New("invalid_virtual_account_no")
	}

	sql := virtualAccount.selectQuery()
	sql += ` where account_no=$1`

	cursor, err := virtualAccount.Query(sql, accountNo)
	if err != nil {
		return nil, err
	}

	if cursor.Next() {
		b, err := virtualAccount.fetchRow(cursor)
		if err != nil {
			return nil, err
		}
		return &b, nil
	}

	return nil, errors.New("virtual account tidak ditemukan")

}

func (virtualAccount VirtualAccount) FindNoHp(noHp string) (*VirtualAccount, error) {

	if helpers.Empty(noHp) {
		return nil, errors.New("invalid_no_hp")
	}

	// sql := virtualAccount.selectQuery()
	// sql += ` where no_hp=$1`

	sql := `select * from virtual_accounts where no_hp=$1`

	cursor, err := virtualAccount.Query(sql, noHp)
	if err != nil {
		return nil, err
	}

	if cursor.Next() {
		b, err := virtualAccount.fetchRow(cursor)
		if err != nil {
			return nil, err
		}
		return &b, nil
	}

	return nil, errors.New("virtual account tidak ditemukan")

}

func (virtualAccount VirtualAccount) Update(
	accountNo string, noHp string, email string,
	accountName string, pin string, password string,
	saldo float64, seqNo string) error {

	sql := `update virtual_accounts
	set no_hp=$2, email=$3,
	account_name=$4, pin=$5,	
	password=$6, saldo=$7,	
	seq_no=$8, updated_at=$9 
	where account_no=$1`

	enPassw, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	enPin := base64.StdEncoding.EncodeToString([]byte(pin))

	result, err := virtualAccount.Exec(sql,
		accountNo, noHp, email,
		accountName, enPassw, enPin,
		saldo, seqNo, time.Now().UTC())

	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("VirtualAccount.Update.AffectedRows=", affectedRows)

	return nil

}

//hapus file by virtualAccount ID
func (virtualAccount VirtualAccount) Remove(virtualAccountNo string) error {

	sql := "delete from virtual_accounts where account_no=$1"
	result, err := virtualAccount.Exec(sql, virtualAccountNo)
	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("VirtualAccount.Remove.AffectedRows=", affectedRows)
	return nil
}

//hapus semua file
func (virtualAccount VirtualAccount) Truncate() error {

	sql := `delete from virtual_accounts`
	result, err := virtualAccount.Exec(sql)
	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("VirtualAccount.Truncate.AffectedRows=", affectedRows)
	return nil

}
