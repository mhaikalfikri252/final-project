package datasql

import (
	"database/sql"
	"errors"
	"log"
	"time"
	"final-project/helpers"

	_ "github.com/lib/pq"
)

type BankAccount struct {
	BaseDataSql

	BankAccountNo    string  `json:"bankAccountNo"`
	BankAccountOwner string  `json:"bankAccountOwner"`
	Saldo            float64 `json:"saldo"`

	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt *time.Time `json:"updatedAt"`
}

func CreateBankAccount(db *sql.DB) BankAccount {
	bankAccount := BankAccount{}
	bankAccount.DB = db
	return bankAccount
}

func CreateBankAccountWithTransaction(transaction *sql.Tx) BankAccount {
	bankAccount := BankAccount{}
	bankAccount.Transaction = transaction
	bankAccount.UseTransaction = true
	return bankAccount
}

func (bankAccount BankAccount) Add(
	bankAccountNo string, bankAccountOwner string, saldo float64) error {

	if helpers.Empty(bankAccountNo) {
		return errors.New("invalid_bank_account_no")
	}

	if helpers.Empty(bankAccountOwner) {
		return errors.New("invalid_bank_account_owner")
	}

	if !helpers.Float64Gtz(saldo) {
		return errors.New("invalid_saldo")
	}

	sql := `insert into bank_accounts (
		bank_account_no, 
		bank_account_owner,
		saldo,
		created_at) values ($1, $2, $3 ,$4)`

	result, err := bankAccount.Exec(sql,
		bankAccountNo,
		bankAccountOwner,
		saldo,
		time.Now().UTC())

	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("BankAccount.Add.AffectedRows=", affectedRows)
	return nil

}

func (bankAccount BankAccount) fetchRow(cursor *sql.Rows) (BankAccount, error) {
	b := BankAccount{}
	err := cursor.Scan(
		&b.BankAccountNo,
		&b.BankAccountOwner,
		&b.Saldo,
		&b.CreatedAt,
		&b.UpdatedAt)
	if err != nil {
		return BankAccount{}, err
	}
	return b, nil
}

func (bankAccount BankAccount) selectQuery() string {
	sql := `select bank_account_no, bank_account_owner, saldo, created_at, updated_at from bank_accounts`
	return sql
}

func (bankAccount BankAccount) GetBankAccounts() ([]BankAccount, error) {

	sql := bankAccount.selectQuery()
	cursor, err := bankAccount.Query(sql)
	if err != nil {
		return nil, err
	}

	bankAccounts := []BankAccount{}

	for cursor.Next() {
		b, err := bankAccount.fetchRow(cursor)
		if err != nil {
			return nil, err
		}
		bankAccounts = append(bankAccounts, b)
	}

	return bankAccounts, nil
}

func (bankAccount BankAccount) FindOne(bankAccountNo string) (*BankAccount, error) {

	if helpers.Empty(bankAccountNo) {
		return nil, errors.New("invalid_bankAccount_no")
	}

	sql := bankAccount.selectQuery()
	sql += ` where bank_account_no=$1`

	cursor, err := bankAccount.Query(sql, bankAccountNo)
	if err != nil {
		return nil, err
	}

	if cursor.Next() {
		b, err := bankAccount.fetchRow(cursor)
		if err != nil {
			return nil, err
		}
		return &b, nil
	}

	return nil, errors.New("bank account tidak ditemukan")

}

//untuk mengupdate data berdasarkan bankAccountNo
func (bankAccount BankAccount) Update(
	bankAccountNo string, 
	bankAccountOwner string, 
	saldo float64) error {

	sql := `update bank_accounts 
	set bank_account_owner=$2,
	saldo=$3,
	updated_at=$4 
	
	where bank_account_no=$1
	`

	result, err := bankAccount.Exec(sql, bankAccountNo, bankAccountOwner, saldo, time.Now().UTC())
	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("BankAccount.Update.AffectedRows=", affectedRows)

	return nil

}

//hapus file by bankAccount ID
func (bankAccount BankAccount) Remove(bankAccountNo string) error {

	sql := "delete from bank_accounts where bank_account_no=$1"
	result, err := bankAccount.Exec(sql, bankAccountNo)
	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("BankAccount.Remove.AffectedRows=", affectedRows)
	return nil
}

//hapus semua file
func (bankAccount BankAccount) Truncate() error {

	sql := `delete from bank_accounts`
	result, err := bankAccount.Exec(sql)
	if err != nil {
		return err
	}

	affectedRows, err := result.RowsAffected()
	if err != nil {
		return err
	}

	log.Println("BankAccount.Truncate.AffectedRows=", affectedRows)
	return nil

}
