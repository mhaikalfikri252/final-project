package datanosql

import (
	"context"
	"log"
	"testing"
	"final-project/connection"
	"final-project/helpers"
)

func TestTransfer_AddTransfer(t *testing.T) {

	ctx := context.TODO()

	conn, err := connection.OpenMongoDb(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer connection.CloseMongoDb(ctx, conn)

	transferNoSql := CreateTransfer(conn)
	err = transferNoSql.Truncate(ctx)
	if err != nil {
		t.Fatal(err)
	}

	srcVaAccountNo := "827952"
	srcVaAccountName := "Halah"
	destVaAccountNo := "5285734"
	destVaAccountName := "Gatau"
	transferAmount := float64(340000)

	transfer, err := transferNoSql.AddTransfer(ctx,
		srcVaAccountNo, srcVaAccountName,
		destVaAccountNo, destVaAccountName,
		transferAmount)

	if err != nil {
		t.Fatal(err)
	}

	transfers, err := transferNoSql.ListTransfers(ctx)
	if err != nil {
		t.Fatal(err)
	}
	log.Println("listTransfers=", helpers.ToJson(transfers))

	err = transferNoSql.FindOne(ctx, transfer.TransferID)
	if err != nil {
		t.Fatal(err)
	}

	log.Println(helpers.ToJson(transferNoSql))

	// UPDATE
	srcVaAccountNo = "4123423"
	srcVaAccountName = "wadi"
	destVaAccountNo = "31224512"
	destVaAccountName = "Reom"
	transferAmount = float64(312000)

	err = transferNoSql.Update(ctx, transfer.TransferID,
		srcVaAccountNo, srcVaAccountName,
		destVaAccountNo, destVaAccountName,
		transferAmount)

	if err != nil {
		t.Fatal(err)
	}
	log.Println(helpers.ToJson(transferNoSql))

	if transferNoSql.SrcVaAccountNo != srcVaAccountNo {
		t.Fatal("Expected=", srcVaAccountName, "actual=", transferNoSql.SrcVaAccountNo)
	}
	if transferNoSql.SrcVaAccountName != srcVaAccountName {
		t.Fatal("Expected=", srcVaAccountName, "actual=", transferNoSql.SrcVaAccountName)
	}
	if transferNoSql.DestVaAccountNo != destVaAccountNo {
		t.Fatal("Expected=", destVaAccountNo, "actual=", transferNoSql.DestVaAccountNo)
	}
	if transferNoSql.DestVaAccountName != destVaAccountName {
		t.Fatal("Expected=", destVaAccountName, "actual=", transferNoSql.DestVaAccountName)
	}
	if transferNoSql.TransferAmount != transferAmount {
		t.Fatal("Expected=", transferAmount, "actual=", transferNoSql.TransferAmount)
	}

	// DELETE BY ID
	// err = transferNoSql.Delete(ctx, transferNoSql.TransferID)
	// if err != nil {
	// 	t.Fatal(err)
	// }

}
