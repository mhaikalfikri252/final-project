package main

import (
	"log"
	"net/http"

	"final-project/apis"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)
	router := mux.NewRouter()

	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Welcome server"))
	}).Methods("GET")

	// login
	loginApi := apis.LoginApi{}
	router.HandleFunc("/logins", loginApi.Login).Methods("GET")
	router.HandleFunc("/logouts", loginApi.Logout).Methods("POST")

	// virtual account
	virtualAccountApi := apis.VirtualAccountApi{}
	router.HandleFunc("/virtualaccounts", virtualAccountApi.GetVirtualAccounts).Methods("GET")
	router.HandleFunc("/virtualaccount", virtualAccountApi.GetVirtualAccountByID).Methods("GET")
	router.HandleFunc("/virtualaccount/create", virtualAccountApi.PostVirtualAccount).Methods("POST")
	router.HandleFunc("/virtualaccount/update", virtualAccountApi.UpdateVirtualAccount).Methods("PUT")
	router.HandleFunc("/virtualaccount/delete", virtualAccountApi.RemoveVirtualAccount).Methods("DELETE")

	// topup
	topUpApi := apis.TopUpApi{}
	router.HandleFunc("/topups", topUpApi.GetTopUps).Methods("GET")
	router.HandleFunc("/topup", topUpApi.GetTopUpByID).Methods("GET")
	router.HandleFunc("/topup/create", topUpApi.PostTopUp).Methods("POST")
	router.HandleFunc("/topup/update", topUpApi.UpdateTopUp).Methods("PUT")
	router.HandleFunc("/topup/delete", topUpApi.RemoveTopUp).Methods("DELETE")

	// transfer
	transferApi := apis.TransferApi{}
	router.HandleFunc("/transfers", transferApi.GetTransfers).Methods("GET")
	router.HandleFunc("/transfer", transferApi.GetTransferByID).Methods("GET")
	router.HandleFunc("/transfer/create", transferApi.PostTransfer).Methods("POST")
	router.HandleFunc("/transfer/update", transferApi.UpdateTransfer).Methods("PUT")
	router.HandleFunc("/transfer/delete", transferApi.RemoveTransfer).Methods("DELETE")

	// pay
	payApi := apis.PayApi{}
	router.HandleFunc("/pays", payApi.GetPays).Methods("GET")
	router.HandleFunc("/pay", payApi.GetPayByID).Methods("GET")
	router.HandleFunc("/pay/create", payApi.PostPay).Methods("POST")
	router.HandleFunc("/pay/update", payApi.UpdatePay).Methods("PUT")
	router.HandleFunc("/pay/delete", payApi.RemovePay).Methods("DELETE")

	// virtual account transaction
	VaTransactionApi := apis.VaTransactionApi{}
	router.HandleFunc("/vatransactions", VaTransactionApi.GetVaTransactions).Methods("GET")
	router.HandleFunc("/vatransaction", VaTransactionApi.GetVaTransactionByID).Methods("GET")
	router.HandleFunc("/vatransaction/create", VaTransactionApi.PostVaTransaction).Methods("POST")
	router.HandleFunc("/vatransaction/update", VaTransactionApi.UpdateVaTransaction).Methods("PUT")
	router.HandleFunc("/vatransaction/delete", VaTransactionApi.RemoveVaTransaction).Methods("DELETE")

	// bank account
	bankAccountApi := apis.BankAccountApi{}
	router.HandleFunc("/bankaccounts", bankAccountApi.GetBankAccounts).Methods("GET")
	router.HandleFunc("/bankaccount", bankAccountApi.GetBankAccountByID).Methods("GET")
	router.HandleFunc("/bankaccount/create", bankAccountApi.PostBankAccount).Methods("POST")
	router.HandleFunc("/bankaccount/update", bankAccountApi.UpdateBankAccount).Methods("PUT")
	router.HandleFunc("/bankaccount/delete", bankAccountApi.RemoveBankAccount).Methods("DELETE")

	// bank transaction
	bankTransactionApi := apis.BankTransactionApi{}
	router.HandleFunc("/banktransactions", bankTransactionApi.GetBankTransactions).Methods("GET")
	router.HandleFunc("/banktransaction", bankTransactionApi.GetBankTransactionByID).Methods("GET")
	router.HandleFunc("/banktransaction/create", bankTransactionApi.PostBankTransaction).Methods("POST")
	router.HandleFunc("/banktransaction/update", bankTransactionApi.UpdateBankTransaction).Methods("PUT")
	router.HandleFunc("/banktransaction/delete", bankTransactionApi.RemoveBankTransaction).Methods("DELETE")

	//standard
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})

	log.Println("Server listening to 127.0.0.1:8000")
	err := http.ListenAndServe("127.0.0.1:8000", handlers.CORS(headers, methods, origins)(router))
	if err != nil {
		log.Fatal(err)
	}
}
