package datanosql

import (
	"context"
	"final-project/connection"
	"final-project/helpers"
	"log"
	"testing"
)

func TestVaTransaction_AddVaTransaction(t *testing.T) {

	ctx := context.TODO()

	conn, err := connection.OpenMongoDb(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer connection.CloseMongoDb(ctx, conn)

	vaTransactionNoSql := CreateVaTransaction(conn)
	err = vaTransactionNoSql.Truncate(ctx)
	if err != nil {
		t.Fatal(err)
	}

	vaAccountNo := "13134131"
	vaAccountName := "Kalia"
	transactionAmount := float64(628000)
	transactionType := TRANSACTION_TYPE_PAY
	description := "buat bayar sepeda"
	transactionID := "123124"

	vaTransaction, err := vaTransactionNoSql.AddVaTransaction(ctx,
		vaAccountNo, vaAccountName,
		transactionAmount, transactionType,
		description, transactionID)

	if err != nil {
		t.Fatal(err)
	}

	vaTransactions, err := vaTransactionNoSql.ListVaTransactions(ctx)
	if err != nil {
		t.Fatal(err)
	}
	log.Println("listVaTransactions=", helpers.ToJson(vaTransactions))

	err = vaTransactionNoSql.FindOne(ctx, vaTransaction.HistoryID.Hex())
	if err != nil {
		t.Fatal(err)
	}

	log.Println(helpers.ToJson(vaTransactionNoSql))

	// UPDATE
	vaAccountNo = "1341241"
	vaAccountName = "Amui"
	transactionAmount = float64(534000)
	transactionType = TRANSACTION_TYPE_TRANSFER
	description = "buat bayar baju"
	transactionID = "315112"

	err = vaTransactionNoSql.Update(ctx, vaTransaction.HistoryID.Hex(),
		vaAccountNo, vaAccountName,
		transactionAmount, transactionType,
		description, transactionID)

	if err != nil {
		t.Fatal(err)
	}
	log.Println(helpers.ToJson(vaTransactionNoSql))

	if vaTransactionNoSql.VaAccountNo != vaAccountNo {
		t.Fatal("Expected=", vaAccountName, "actual=", vaTransactionNoSql.VaAccountNo)
	}
	if vaTransactionNoSql.VaAccountName != vaAccountName {
		t.Fatal("Expected=", vaAccountName, "actual=", vaTransactionNoSql.VaAccountName)
	}
	if vaTransactionNoSql.TransactionAmount != transactionAmount {
		t.Fatal("Expected=", transactionAmount, "actual=", vaTransactionNoSql.TransactionAmount)
	}
	if vaTransactionNoSql.TransactionType != transactionType {
		t.Fatal("Expected=", transactionType, "actual=", vaTransactionNoSql.TransactionType)
	}
	if vaTransactionNoSql.Description != description {
		t.Fatal("Expected=", description, "actual=", vaTransactionNoSql.Description)
	}
	if vaTransactionNoSql.TransactionID != transactionID {
		t.Fatal("Expected=", transactionID, "actual=", vaTransactionNoSql.TransactionID)
	}
	// DELETE BY ID
	// err = vaTransactionNoSql.Delete(ctx, vaTransactionNoSql.HistoryID.Hex())
	// if err != nil {
	// 	t.Fatal(err)
	// }

}
