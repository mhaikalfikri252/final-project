package apis

import (
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/datasql"
	"net/http"
)

type VirtualAccountApi struct {
	BaseApi

	datasql.VirtualAccount
}

func (virtualAccountApi VirtualAccountApi) GetVirtualAccounts(w http.ResponseWriter, r *http.Request) {

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	vaModel := datasql.CreateVirtualAccount(db)
	virtualAccounts, err := vaModel.GetVirtualAccounts()
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	} else {
		virtualAccountApi.Json(w, virtualAccounts, http.StatusOK)
		return
	}

}

func (virtualAccountApi VirtualAccountApi) GetVirtualAccountByID(w http.ResponseWriter, r *http.Request) {

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	virtualAccountID := virtualAccountApi.QueryParam(r, "accountNo")

	vaModel := datasql.CreateVirtualAccount(db)
	virtualAccount, err := vaModel.FindOne(virtualAccountID)
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	} else {
		virtualAccountApi.Json(w, virtualAccount, http.StatusOK)
		return
	}

}

func (virtualAccountApi VirtualAccountApi) PostVirtualAccount(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&virtualAccountApi)
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	}

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	vaModel := datasql.CreateVirtualAccount(db)

	err = vaModel.Add(
		virtualAccountApi.AccountNo,
		virtualAccountApi.NoHp,
		virtualAccountApi.Email,
		virtualAccountApi.AccountName,
		virtualAccountApi.Pin,
		virtualAccountApi.Password,
		virtualAccountApi.Saldo,
		virtualAccountApi.SeqNo)
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	} else {

		virtualAccount, err := vaModel.FindOne(virtualAccountApi.AccountNo)
		if err != nil {
			virtualAccountApi.Error(w, err)
			return
		} else {
			virtualAccountApi.Json(w, virtualAccount, http.StatusOK)
			return
		}
	}

}

func (virtualAccountApi VirtualAccountApi) UpdateVirtualAccount(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&virtualAccountApi)
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	}

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	vaModel := datasql.CreateVirtualAccount(db)

	err = vaModel.Update(virtualAccountApi.AccountNo,
		virtualAccountApi.NoHp,
		virtualAccountApi.Email,
		virtualAccountApi.AccountName,
		virtualAccountApi.Pin,
		virtualAccountApi.Password,
		virtualAccountApi.Saldo,
		virtualAccountApi.SeqNo)
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	} else {

		virtualAccount, err := vaModel.FindOne(virtualAccountApi.AccountNo)
		if err != nil {
			virtualAccountApi.Error(w, err)
			return
		} else {
			virtualAccountApi.Json(w, virtualAccount, http.StatusOK)
			return
		}
	}
}

func (virtualAccountApi VirtualAccountApi) RemoveVirtualAccount(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&virtualAccountApi)
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	}

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	vaModel := datasql.CreateVirtualAccount(db)

	err = vaModel.Remove(virtualAccountApi.AccountNo)
	if err != nil {
		virtualAccountApi.Error(w, err)
		return
	} else {
		virtualAccountApi.Empty(w, http.StatusOK)
		return
	}

}
