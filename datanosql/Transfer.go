package datanosql

import (
	"context"
	"errors"
	"final-project/config"
	"log"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Transfer struct {
	BaseNoSql `bson:"-"`

	TransferID        string    `json:"transferID" bson:"_transferID"`
	SrcVaAccountNo    string    `json:"srcVaAccountNo" bson:"srcVaAccountNo"`
	SrcVaAccountName  string    `json:"srcVaAccountName" bson:"srcVaAccountName"`
	DestVaAccountNo   string    `json:"destVaAccountNo" bson:"destVaAccountNo"`
	DestVaAccountName string    `json:"destVaAccountName" bson:"destVaAccountName"`
	TransferAmount    float64   `json:"transferAmount" bson:"transferAmount"`
	CreatedAt         time.Time `json:"createdAt" bson:"createdAt"`
}

func CreateTransfer(client *mongo.Client) Transfer {
	transfer := Transfer{}
	transfer.Client = client
	return transfer
}

func (transfer Transfer) Collection() *mongo.Collection {
	session, _ := transfer.Client.StartSession()
	session.StartTransaction()

	return transfer.Client.Database(config.DATABASE).Collection("transfers")
}

func (transfer Transfer) Truncate(ctx context.Context) error {
	return transfer.Collection().Drop(ctx)
}

func (transfer Transfer) AddTransfer(ctx context.Context,
	srcVaAccountNo string, srcVaAccountName string,
	destVaAccountNo string, destVaAccountName string,
	transferAmount float64) (*Transfer, error) {

	transfer.TransferID = uuid.New().String()
	transfer.SrcVaAccountNo = srcVaAccountNo
	transfer.SrcVaAccountName = srcVaAccountName
	transfer.DestVaAccountNo = destVaAccountNo
	transfer.DestVaAccountName = destVaAccountName
	transfer.TransferAmount = transferAmount
	transfer.CreatedAt = time.Now().UTC()

	_, err := transfer.Collection().InsertOne(ctx, transfer)
	if err != nil {
		return nil, err
	}
	return &transfer, nil

}

func (transfer *Transfer) FindOne(ctx context.Context, transferID string) error {


	filter := bson.D{
		primitive.E{Key: "_transferID", Value: transferID},
	}

	result := transfer.Collection().FindOne(ctx, filter)

	if result.Err() != nil {

		log.Println(result.Err())
		if result.Err().Error() == config.MONGO_NO_DOCUMENT {
			return errors.New("transfer_not_found")
		}
		return result.Err()
	}

	err := result.Decode(&transfer)
	if err != nil {
		return err
	}

	return nil

}

func (transfer *Transfer) Update(ctx context.Context,
	transferID string,
	srcVaAccountNo string, srcVaAccountName string,
	destVaAccountNo string, destVaAccountName string,
	transferAmount float64) error {


	filter := bson.D{
		primitive.E{Key: "_transferID", Value: transferID},
	}

	set := bson.D{
		primitive.E{
			Key: "$set",
			Value: bson.D{
				primitive.E{Key: "srcVaAccountNo", Value: srcVaAccountNo},
				primitive.E{Key: "srcVaAccountName", Value: srcVaAccountName},
				primitive.E{Key: "destVaAccountNo", Value: destVaAccountNo},
				primitive.E{Key: "destVaAccountName", Value: destVaAccountName},
				primitive.E{Key: "transferAmount", Value: transferAmount},
			},
		},
	}

	optionsAfter := options.After
	updateOptions := &options.FindOneAndUpdateOptions{
		ReturnDocument: &optionsAfter,
	}

	result := transfer.Collection().FindOneAndUpdate(ctx, filter, set, updateOptions)
	if result.Err() != nil {
		return result.Err()
	}

	return result.Decode(&transfer)

}

func (transfer Transfer) Delete(ctx context.Context, transferID string) error {


	filter := bson.D{
		primitive.E{Key: "_transferID", Value: transferID},
	}

	result, err := transfer.Collection().DeleteOne(ctx, filter, nil)
	if err != nil {
		return err
	}
	log.Println(result)
	return nil

}

func (transfer Transfer) ListTransfers(ctx context.Context) ([]Transfer, error) {

	transfers := []Transfer{}
	filter := bson.D{}
	cursor, err := transfer.Collection().Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(ctx, &transfers)
	if err != nil {
		return nil, err
	}

	return transfers, nil

}
