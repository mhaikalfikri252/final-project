package connection

import (
	"testing"
	"final-project/config"
)

func TestConnection_OpenConnection(t *testing.T) {

	
	db := OpenConnection(config.CURRENT_PHASE)
	defer db.Close()

	err := db.Ping()
	if err != nil {
		t.Fatal("ping error", err.Error())
	}

}
