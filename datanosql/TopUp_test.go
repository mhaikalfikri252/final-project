package datanosql

import (
	"context"
	"log"
	"testing"
	"final-project/connection"
	"final-project/helpers"
)

func TestTopUp_AddTopUp(t *testing.T) {

	ctx := context.TODO()

	conn, err := connection.OpenMongoDb(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer connection.CloseMongoDb(ctx, conn)

	topUpNoSql := CreateTopUp(conn)
	err = topUpNoSql.Truncate(ctx)
	if err != nil {
		t.Fatal(err)
	}

	bankCode := "123456"
	bankName := "BRI"
	vaAccountNo := "436645"
	vaAccountName := "Mamui"
	topUpAmount := float64(89000)

	topUp, err := topUpNoSql.AddTopUp(ctx,
		bankCode, bankName,
		vaAccountNo, vaAccountName,
		topUpAmount)

	if err != nil {
		t.Fatal(err)
	}

	topUps, err := topUpNoSql.ListTopUps(ctx)
	if err != nil {
		t.Fatal(err)
	}
	log.Println("listTopUps=", helpers.ToJson(topUps))

	err = topUpNoSql.FindOne(ctx, topUp.TopUpID)
	if err != nil {
		t.Fatal(err)
	}

	log.Println(helpers.ToJson(topUpNoSql))

	// UPDATE
	bankCode = "56345"
	bankName = "BSI"
	vaAccountNo = "142541"
	vaAccountName = "Meimei"
	topUpAmount = float64(12000)

	err = topUpNoSql.Update(ctx, topUp.TopUpID,
		bankCode, bankName,
		vaAccountNo, vaAccountName,
		topUpAmount)

	if err != nil {
		t.Fatal(err)
	}
	log.Println(helpers.ToJson(topUpNoSql))

	if topUpNoSql.BankCode != bankCode {
		t.Fatal("Expected=", bankCode, "actual=", topUpNoSql.BankCode)
	}
	if topUpNoSql.BankName != bankName {
		t.Fatal("Expected=", bankName, "actual=", topUpNoSql.BankName)
	}
	if topUpNoSql.VaAccountNo != vaAccountNo {
		t.Fatal("Expected=", vaAccountNo, "actual=", topUpNoSql.VaAccountNo)
	}
	if topUpNoSql.VaAccountName != vaAccountName {
		t.Fatal("Expected=", vaAccountName, "actual=", topUpNoSql.VaAccountName)
	}
	if topUpNoSql.TopUpAmount != topUpAmount {
		t.Fatal("Expected=", topUpAmount, "actual=", topUpNoSql.TopUpAmount)
	}

	// DELETE BY ID
	// err = topUpNoSql.Delete(ctx, topUpNoSql.TopUpID)
	// if err != nil {
	// 	t.Fatal(err)
	// }

}
