package datanosql

import (
	"context"
	"errors"
	"log"
	"time"
	"final-project/config"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Pay struct {
	BaseNoSql `bson:"-"`

	PayID                 primitive.ObjectID `json:"payID" bson:"_payID"`
	MerchantVaAccountNo   string                `json:"merchantVaAccountNo" bson:"merchantVaAccountNo"`
	MerchantVaAccountName string             `json:"merchantVaAccountName" bson:"merchantVaAccountName"`
	SrcVaAccountNo        string                `json:"srcVaAccountNo" bson:"srcVaAccountNo"`
	SrcVaAccountName      string             `json:"srcVaAccountName" bson:"srcVaAccountName"`
	PayAmount             float64            `json:"payAmount" bson:"payAmount"`
	CreatedAt             time.Time          `json:"createdAt" bson:"createdAt"`
}

func CreatePay(client *mongo.Client) Pay {
	pay := Pay{}
	pay.Client = client
	return pay
}

func (pay Pay) Collection() *mongo.Collection {
	session, _ := pay.Client.StartSession()
	session.StartTransaction()

	return pay.Client.Database(config.DATABASE).Collection("pays")
}

func (pay Pay) Truncate(ctx context.Context) error {
	return pay.Collection().Drop(ctx)
}

func (pay Pay) AddPay(ctx context.Context,
	merchantVaAccountNo string, merchantVaAccountName string,
	srcVaAccountNo string, srcVaAccountName string,
	payAmount float64) (*Pay, error) {

	pay.PayID = primitive.NewObjectID()
	pay.MerchantVaAccountNo = merchantVaAccountNo
	pay.MerchantVaAccountName = merchantVaAccountName
	pay.SrcVaAccountNo = srcVaAccountNo
	pay.SrcVaAccountName = srcVaAccountName
	pay.PayAmount = payAmount
	pay.CreatedAt = time.Now().UTC()

	_, err := pay.Collection().InsertOne(ctx, pay)
	if err != nil {
		return nil, err
	}
	return &pay, nil

}

func (pay *Pay) FindOne(ctx context.Context, payID string) error {

	oid, err := primitive.ObjectIDFromHex(payID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_payID", Value: oid},
	}

	result := pay.Collection().FindOne(ctx, filter)

	if result.Err() != nil {

		log.Println(result.Err())
		if result.Err().Error() == config.MONGO_NO_DOCUMENT {
			return errors.New("pay_not_found")
		}
		return result.Err()
	}

	err = result.Decode(&pay)
	if err != nil {
		return err
	}

	return nil

}

func (pay *Pay) Update(ctx context.Context,
	payID string,
	merchantVaAccountNo string, merchantVaAccountName string,
	srcVaAccountNo string, srcVaAccountName string,
	payAmount float64) error {

	oid, err := primitive.ObjectIDFromHex(payID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_payID", Value: oid},
	}

	set := bson.D{
		primitive.E{
			Key: "$set",
			Value: bson.D{
				primitive.E{Key: "merchantVaAccountNo", Value: merchantVaAccountNo},
				primitive.E{Key: "merchantVaAccountName", Value: merchantVaAccountName},
				primitive.E{Key: "srcVaAccountNo", Value: srcVaAccountNo},
				primitive.E{Key: "srcVaAccountName", Value: srcVaAccountName},
				primitive.E{Key: "payAmount", Value: payAmount},
			},
		},
	}

	optionsAfter := options.After
	updateOptions := &options.FindOneAndUpdateOptions{
		ReturnDocument: &optionsAfter,
	}

	result := pay.Collection().FindOneAndUpdate(ctx, filter, set, updateOptions)
	if result.Err() != nil {
		return result.Err()
	}

	return result.Decode(&pay)

}

func (pay Pay) Delete(ctx context.Context, payID string) error {

	oid, err := primitive.ObjectIDFromHex(payID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_payID", Value: oid},
	}

	result, err := pay.Collection().DeleteOne(ctx, filter, nil)
	if err != nil {
		return err
	}
	log.Println(result)
	return nil

}

func (pay Pay) ListPays(ctx context.Context) ([]Pay, error) {

	pays := []Pay{}
	filter := bson.D{}
	cursor, err := pay.Collection().Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(ctx, &pays)
	if err != nil {
		return nil, err
	}

	return pays, nil

}
