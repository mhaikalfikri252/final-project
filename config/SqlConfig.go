package config


const (
	DEV           = "DEV"
	STAGING       = "STAGING"
	PROD          = "PROD"
	CURRENT_PHASE = DEV
)

type DbConfig struct {
	Driver   string
	User     string
	Password string
	DbName   string
	SslMode  string
}

var DB_CONFIGS map[string]DbConfig = map[string]DbConfig{
	DEV: {
		Driver:   "postgres",
		User:     "postgres",
		Password: "postgres",
		DbName:   "dbfinal",
		SslMode:  "disable",
	},

	STAGING: {
		Driver:   "postgres",
		User:     "postgres",
		Password: "postgres",
		DbName:   "dbfinal",
		SslMode:  "disable",
	},

	PROD: {
		Driver:   "postgres",
		User:     "postgres",
		Password: "postgres",
		DbName:   "dbfinal",
		SslMode:  "disable",
	},
}