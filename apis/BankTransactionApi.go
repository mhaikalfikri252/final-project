package apis

import (
	"context"
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/datanosql"
	"net/http"
)

type BankTransactionApi struct {
	BaseApi
	datanosql.BankTransaction

	TransactionID string `json:"transactionID"`
}

func (bankTransactionApi BankTransactionApi) GetBankTransactions(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = bankTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	bankTransactionModel := datanosql.CreateBankTransaction(mongoClient)
	bankTransactions, err := bankTransactionModel.ListBankTransactions(ctx)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	} else {
		bankTransactionApi.Json(w, bankTransactions, http.StatusOK)
		return
	}
}

func (bankTransactionApi BankTransactionApi) GetBankTransactionByID(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = bankTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	transactionID := bankTransactionApi.QueryParam(r, "transactionID")

	bankTransactionModel := datanosql.CreateBankTransaction(mongoClient)
	err = bankTransactionModel.FindOne(ctx, transactionID)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	} else {
		bankTransactionApi.Json(w, bankTransactionModel, http.StatusOK)
		return
	}

}

func (bankTransactionApi BankTransactionApi) PostBankTransaction(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bankTransactionApi)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = bankTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	bankTransactionModel := datanosql.CreateBankTransaction(mongoClient)
	bankTransaction, err := bankTransactionModel.AddBankTransaction(ctx,
		bankTransactionApi.BankAccountNo,
		bankTransactionApi.BankAccountOwner,
		bankTransactionApi.TransactionAmount,
		bankTransactionApi.Reference)

	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	} else {
		bankTransactionApi.Json(w, bankTransaction, http.StatusOK)
		return
	}
}

func (bankTransactionApi BankTransactionApi) UpdateBankTransaction(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bankTransactionApi)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = bankTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	bankTransactionModel := datanosql.CreateBankTransaction(mongoClient)
	err = bankTransactionModel.Update(ctx, 
		bankTransactionApi.TransactionID,
		bankTransactionApi.BankAccountNo,
		bankTransactionApi.BankAccountOwner,
		bankTransactionApi.TransactionAmount,
		bankTransactionApi.Reference)
		
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	} else {
		bankTransactionApi.Json(w, bankTransactionModel, http.StatusOK)
		return
	}

}

func (bankTransactionApi BankTransactionApi) RemoveBankTransaction(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bankTransactionApi)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = bankTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	}

	bankTransactionModel := datanosql.CreateBankTransaction(mongoClient)
	err = bankTransactionModel.Delete(ctx, bankTransactionApi.TransactionID)
	if err != nil {
		bankTransactionApi.Error(w, err)
		return
	} else {
		bankTransactionApi.Json(w, bankTransactionModel, http.StatusOK)
		return
	}
}
