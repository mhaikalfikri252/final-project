package datanosql

import (
	"context"
	"log"
	"testing"
	"final-project/connection"
	"final-project/helpers"
)

func TestPay_AddPay(t *testing.T) {

	ctx := context.TODO()

	conn, err := connection.OpenMongoDb(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer connection.CloseMongoDb(ctx, conn)

	payNoSql := CreatePay(conn)
	err = payNoSql.Truncate(ctx)
	if err != nil {
		t.Fatal(err)
	}

	merchantVaAccountNo := "012345"
	merchantVaAccountName := "Alpha"
	srcVaAccountNo := "09878"
	srcVaAccountName := "Homdeh"
	payAmount := float64(190000)

	pay, err := payNoSql.AddPay(ctx,
		merchantVaAccountNo, merchantVaAccountName,
		srcVaAccountNo, srcVaAccountName,
		payAmount)

	if err != nil {
		t.Fatal(err)
	}

	pays, err := payNoSql.ListPays(ctx)
	if err != nil {
		t.Fatal(err)
	}
	log.Println("listPays=", helpers.ToJson(pays))

	err = payNoSql.FindOne(ctx, pay.PayID.Hex())
	if err != nil {
		t.Fatal(err)
	}

	log.Println(helpers.ToJson(payNoSql))

	// UPDATE
	merchantVaAccountNo = "534514"
	merchantVaAccountName = "WalkPay"
	srcVaAccountNo = "74567"
	srcVaAccountName = "Merh"
	payAmount = float64(16000)

	err = payNoSql.Update(ctx, pay.PayID.Hex(),
		merchantVaAccountNo, merchantVaAccountName,
		srcVaAccountNo, srcVaAccountName,
		payAmount)

	if err != nil {
		t.Fatal(err)
	}
	log.Println(helpers.ToJson(payNoSql))

	if payNoSql.MerchantVaAccountNo != merchantVaAccountNo {
		t.Fatal("Expected=", merchantVaAccountNo, "actual=", payNoSql.MerchantVaAccountNo)
	}
	if payNoSql.MerchantVaAccountName != merchantVaAccountName {
		t.Fatal("Expected=", merchantVaAccountName, "actual=", payNoSql.MerchantVaAccountName)
	}
	if payNoSql.SrcVaAccountNo != srcVaAccountNo {
		t.Fatal("Expected=", srcVaAccountNo, "actual=", payNoSql.SrcVaAccountNo)
	}
	if payNoSql.SrcVaAccountName != srcVaAccountName {
		t.Fatal("Expected=", srcVaAccountName, "actual=", payNoSql.SrcVaAccountName)
	}
	if payNoSql.PayAmount != payAmount {
		t.Fatal("Expected=", payAmount, "actual=", payNoSql.SrcVaAccountName)
	}

	// DELETE BY ID
	// err = payNoSql.Delete(ctx, payNoSql.PayID.Hex())
	// if err != nil {
	// 	t.Fatal(err)
	// }

}
