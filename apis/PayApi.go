package apis

import (
	"context"
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/datanosql"
	"net/http"
)

type PayApi struct {
	BaseApi
	datanosql.Pay

	PayID string `json:"payID"`
}

func (payApi PayApi) GetPays(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		payApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = payApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	payModel := datanosql.CreatePay(mongoClient)
	pays, err := payModel.ListPays(ctx)
	if err != nil {
		payApi.Error(w, err)
		return
	} else {
		payApi.Json(w, pays, http.StatusOK)
		return
	}
}

func (payApi PayApi) GetPayByID(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		payApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = payApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	payID := payApi.QueryParam(r, "payID")

	payModel := datanosql.CreatePay(mongoClient)
	err = payModel.FindOne(ctx, payID)
	if err != nil {
		payApi.Error(w, err)
		return
	} else {
		payApi.Json(w, payModel, http.StatusOK)
		return
	}

}

func (payApi PayApi) PostPay(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&payApi)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		payApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = payApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	payModel := datanosql.CreatePay(mongoClient)
	pay, err := payModel.AddPay(ctx,
		payApi.MerchantVaAccountNo, payApi.MerchantVaAccountName,
		payApi.SrcVaAccountNo, payApi.SrcVaAccountName,
		payApi.PayAmount)

	if err != nil {
		payApi.Error(w, err)
		return
	} else {
		payApi.Json(w, pay, http.StatusOK)
		return
	}
}

func (payApi PayApi) UpdatePay(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&payApi)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		payApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = payApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	payModel := datanosql.CreatePay(mongoClient)
	err = payModel.Update(ctx, payApi.PayID,
		payApi.MerchantVaAccountNo, payApi.MerchantVaAccountName,
		payApi.SrcVaAccountNo, payApi.SrcVaAccountName,
		payApi.PayAmount)

	if err != nil {
		payApi.Error(w, err)
		return
	} else {
		payApi.Json(w, payModel, http.StatusOK)
		return
	}

}

func (payApi PayApi) RemovePay(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&payApi)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		payApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = payApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		payApi.Error(w, err)
		return
	}

	payModel := datanosql.CreatePay(mongoClient)
	err = payModel.Delete(ctx, payApi.PayID)
	if err != nil {
		payApi.Error(w, err)
		return
	} else {
		payApi.Json(w, payModel, http.StatusOK)
		return
	}
}
