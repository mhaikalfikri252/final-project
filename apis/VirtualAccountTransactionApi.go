package apis

import (
	"context"
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/datanosql"
	"net/http"
)

type VaTransactionApi struct {
	BaseApi
	datanosql.VaTransaction

	HistoryID string `json:"historyID"`
}

func (vaTransactionApi VaTransactionApi) GetVaTransactions(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = vaTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	vaTransactionModel := datanosql.CreateVaTransaction(mongoClient)
	vaTransactions, err := vaTransactionModel.ListVaTransactions(ctx)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	} else {
		vaTransactionApi.Json(w, vaTransactions, http.StatusOK)
		return
	}
}

func (vaTransactionApi VaTransactionApi) GetVaTransactionByID(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = vaTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	historyID := vaTransactionApi.QueryParam(r, "historyID")

	vaTransactionModel := datanosql.CreateVaTransaction(mongoClient)
	err = vaTransactionModel.FindOne(ctx, historyID)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	} else {
		vaTransactionApi.Json(w, vaTransactionModel, http.StatusOK)
		return
	}

}

func (vaTransactionApi VaTransactionApi) PostVaTransaction(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&vaTransactionApi)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = vaTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	vaTransactionModel := datanosql.CreateVaTransaction(mongoClient)
	vaTransaction, err := vaTransactionModel.AddVaTransaction(ctx,
		vaTransactionApi.VaAccountNo, vaTransactionApi.VaAccountName,
		vaTransactionApi.TransactionAmount, vaTransactionApi.TransactionType,
		vaTransactionApi.Description, vaTransactionApi.TransactionID)

	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	} else {
		vaTransactionApi.Json(w, vaTransaction, http.StatusOK)
		return
	}
}

func (vaTransactionApi VaTransactionApi) UpdateVaTransaction(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&vaTransactionApi)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = vaTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	vaTransactionModel := datanosql.CreateVaTransaction(mongoClient)
	err = vaTransactionModel.Update(ctx, vaTransactionApi.HistoryID,
		vaTransactionApi.VaAccountNo, vaTransactionApi.VaAccountName,
		vaTransactionApi.TransactionAmount, vaTransactionApi.TransactionType,
		vaTransactionApi.Description, vaTransactionApi.TransactionID)

	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	} else {
		vaTransactionApi.Json(w, vaTransactionModel, http.StatusOK)
		return
	}

}

func (vaTransactionApi VaTransactionApi) RemoveVaTransaction(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&vaTransactionApi)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = vaTransactionApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	}

	vaTransactionModel := datanosql.CreateVaTransaction(mongoClient)
	err = vaTransactionModel.Delete(ctx, vaTransactionApi.HistoryID)
	if err != nil {
		vaTransactionApi.Error(w, err)
		return
	} else {
		vaTransactionApi.Json(w, vaTransactionModel, http.StatusOK)
		return
	}
}
