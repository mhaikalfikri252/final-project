package apis

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/services"
	"net/http"
	"strings"
)

type LoginApi struct {
	BaseApi

	NoHp     string `json:"noHp"`
	Password string `json:"password"`
}

func (loginApi LoginApi) Login(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&loginApi)
	if err != nil {
		loginApi.Error(w, err)
		return
	}

	authorization := r.Header.Get("Authorization")
	authorization = strings.Replace(authorization, "Basic ", "", -1)
	_, err = base64.StdEncoding.DecodeString(authorization)
	if err != nil {
		loginApi.Error(w, err)
		return
	}
	// auth1 := strings.Split(string(auth), ":")

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		loginApi.Error(w, err)
		return
	}

	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	loginService := services.CreateLoginService(mongoClient, db)
	encodedToken, err := loginService.Login(ctx, loginApi.NoHp, loginApi.Password)
	if err != nil {
		loginApi.Error(w, err)
		return
	} else {

		data := map[string]string{
			"token": encodedToken,
		}
		loginApi.Json(w, data, http.StatusOK)
		return
	}

}

func (loginApi LoginApi) Logout(w http.ResponseWriter, r *http.Request) {
	c := http.Cookie{
		Name:   "token",
		MaxAge: -1}
	http.SetCookie(w, &c)

	w.Write([]byte("Old cookie deleted. Logged out!\n"))
}
