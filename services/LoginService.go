package services

import (
	"context"
	"database/sql"
	"encoding/base64"
	"errors"
	"final-project/datanosql"
	"final-project/datasql"
	"log"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)

type LoginService struct {
	BaseService
}

func CreateLoginService(mongoClient *mongo.Client, db *sql.DB) LoginService {
	loginService := LoginService{}
	loginService.MongoClient = mongoClient
	loginService.DB = db
	return loginService
}

func (service LoginService) Login(ctx context.Context, noHp string, password string) (string, error) {

	vaAccountModel := datasql.CreateVirtualAccount(service.DB)
	vaAccount, err := vaAccountModel.FindNoHp(noHp)
	if err != nil {
		log.Println(err)	
		return "", errors.New("invalid_login")
	}

	err = bcrypt.CompareHashAndPassword([]byte(vaAccount.Password), []byte(password))
	if err != nil {
		return "", err
	}

	id := uuid.New().String()
	now := time.Now().UTC().Format(time.RFC3339Nano)
	token := id + vaAccountModel.AccountNo + now

	encryptedToken, err := bcrypt.GenerateFromPassword([]byte(token), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	encodedToken := base64.StdEncoding.EncodeToString(encryptedToken)

	login := datanosql.CreateLogin(service.MongoClient)
	err = login.AddLogin(ctx,
		encodedToken,
		vaAccount.AccountNo,
		vaAccount.AccountName,
		vaAccount.Password,
		vaAccount.NoHp,
		vaAccount.Pin,
	)

	if err != nil {
		return "", err
	}

	return encodedToken, nil

}

func (service LoginService) ParseToken(ctx context.Context, token string) (*datanosql.Login, error) {

	login := datanosql.CreateLogin(service.MongoClient)
	err := login.FindOneByToken(ctx, token)
	if err != nil {
		return nil, err
	}

	if login.IsExpired {
		return nil, errors.New("login_expired")
	}

	return &login, nil

}
