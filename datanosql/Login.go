package datanosql

import (
	"context"
	"errors"
	"final-project/config"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Login struct {
	BaseNoSql `bson:"-"`

	LoginID     primitive.ObjectID `json:"loginID" bson:"_loginID"`
	Token       string             `json:"token" bson:"token"`
	AccountNo   string             `json:"accountNo" bson:"accountNo"`
	AccountName string             `json:"accountName" bson:"accountName"`
	Password    string             `json:"password" bson:"password"`
	NoHp        string             `json:"noHp" bson:"noHp"`
	Pin         string                `json:"pin" bson:"pin"`
	CreatedAt   time.Time          `json:"createdAt" bson:"createdAt"`
	IsExpired   bool               `json:"isExpired" bson:"isExpired"`
}

//constructor
func CreateLogin(client *mongo.Client) Login {
	login := Login{}
	login.Client = client
	return login
}

func (login Login) Collection() *mongo.Collection {
	return login.Client.Database(config.DATABASE).Collection("logins")
}

func (login Login) Truncate(ctx context.Context) error {
	return login.Collection().Drop(ctx)
}

func (login Login) AddLogin(ctx context.Context, token string,
	accountNo string, accountName string,
	password string, noHp string, pin string) error {

	login.LoginID = primitive.NewObjectID()
	login.Token = token
	login.AccountNo = accountNo
	login.AccountName = accountName
	login.Password = password
	login.NoHp = noHp
	login.Pin = pin
	login.CreatedAt = time.Now().UTC()
	login.IsExpired = false

	_, err := login.Collection().InsertOne(ctx, login)
	if err != nil {
		return err
	}
	return nil

}

func (login *Login) FindOneByToken(ctx context.Context, token string) error {

	filter := bson.D{
		primitive.E{Key: "token", Value: token},
	}

	result := login.Collection().FindOne(ctx, filter)
	if result.Err() != nil {

		log.Println(result.Err())
		if result.Err().Error() == config.MONGO_NO_DOCUMENT {
			return errors.New("login_not_found")
		}
		return result.Err()
	}

	err := result.Decode(&login)
	if err != nil {
		return err
	}

	return nil
}

func (login Login) Delete(ctx context.Context, loginID string) error {

	oid, err := primitive.ObjectIDFromHex(loginID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_loginID", Value: oid},
	}

	result, err := login.Collection().DeleteOne(ctx, filter, nil)
	if err != nil {
		return err
	}
	log.Println(result)
	return nil

}

func (login Login) ListLogins(ctx context.Context) ([]Login, error) {

	logins := []Login{}
	filter := bson.D{}
	cursor, err := login.Collection().Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(ctx, &logins)
	if err != nil {
		return nil, err
	}

	return logins, nil

}
