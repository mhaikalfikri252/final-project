package apis

import (
	"context"
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/datanosql"
	"net/http"
)

type TransferApi struct {
	BaseApi
	datanosql.Transfer

	TransferID string `json:"transferID"`
}

func (transferApi TransferApi) GetTransfers(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		transferApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = transferApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	transferModel := datanosql.CreateTransfer(mongoClient)
	transfers, err := transferModel.ListTransfers(ctx)
	if err != nil {
		transferApi.Error(w, err)
		return
	} else {
		transferApi.Json(w, transfers, http.StatusOK)
		return
	}
}

func (transferApi TransferApi) GetTransferByID(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		transferApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = transferApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	transferID := transferApi.QueryParam(r, "transferID")

	transferModel := datanosql.CreateTransfer(mongoClient)
	err = transferModel.FindOne(ctx, transferID)
	if err != nil {
		transferApi.Error(w, err)
		return
	} else {
		transferApi.Json(w, transferModel, http.StatusOK)
		return
	}

}

func (transferApi TransferApi) PostTransfer(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&transferApi)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		transferApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = transferApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	transferModel := datanosql.CreateTransfer(mongoClient)
	transfer, err := transferModel.AddTransfer(ctx,
		transferApi.SrcVaAccountNo, transferApi.SrcVaAccountName,
		transferApi.DestVaAccountNo, transferApi.DestVaAccountName,
		transferApi.TransferAmount)

	if err != nil {
		transferApi.Error(w, err)
		return
	} else {
		transferApi.Json(w, transfer, http.StatusOK)
		return
	}
}

func (transferApi TransferApi) UpdateTransfer(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&transferApi)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		transferApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = transferApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	transferModel := datanosql.CreateTransfer(mongoClient)
	err = transferModel.Update(ctx, transferApi.TransferID,
		transferApi.SrcVaAccountNo, transferApi.SrcVaAccountName,
		transferApi.DestVaAccountNo, transferApi.DestVaAccountName,
		transferApi.TransferAmount)

	if err != nil {
		transferApi.Error(w, err)
		return
	} else {
		transferApi.Json(w, transferModel, http.StatusOK)
		return
	}

}

func (transferApi TransferApi) RemoveTransfer(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&transferApi)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	if err != nil {
		transferApi.Error(w, err)
		return
	}
	defer connection.CloseMongoDb(ctx, mongoClient)

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	_, err = transferApi.ParseToken(r, ctx, mongoClient, db)
	if err != nil {
		transferApi.Error(w, err)
		return
	}

	transferModel := datanosql.CreateTransfer(mongoClient)
	err = transferModel.Delete(ctx, transferApi.TransferID)
	if err != nil {
		transferApi.Error(w, err)
		return
	} else {
		transferApi.Json(w, transferModel, http.StatusOK)
		return
	}
}
