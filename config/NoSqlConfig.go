package config

const (
	DEVNOSQL     = "DEV"
	STAGINGNOSQL = "STAGING"
	PRODNOSQL    = "PROD"

	CURRENT_PHASENOSQL = DEVNOSQL
)

type MongoConfig struct {
	Host string
	Port string
}

const (
	DATABASE = "dbfinal"
)

var MONGO_CONFIGS map[string]MongoConfig = map[string]MongoConfig{
	DEVNOSQL: {
		Host: "localhost",
		Port: "27017",
	},
}
