package datanosql

import (
	"context"
	"errors"
	"log"
	"time"
	"final-project/config"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type BankTransaction struct {
	BaseNoSql `bson:"-"`

	TransactionID     primitive.ObjectID `json:"transactionID" bson:"_transactionID"`
	BankAccountNo     string                `json:"bankAccountNo" bson:"bankAccountNo"`
	BankAccountOwner  string             `json:"bankAccountOwner" bson:"bankAccountOwner"`
	TransactionAmount float64            `json:"transactionAmount" bson:"transactionAmount"`
	Reference         string             `json:"reference" bson:"reference"`
	CreatedAt         time.Time          `json:"createdAt" bson:"createdAt"`
}

func CreateBankTransaction(client *mongo.Client) BankTransaction {
	bankTransaction := BankTransaction{}
	bankTransaction.Client = client
	return bankTransaction
}

func (bankTransaction BankTransaction) Collection() *mongo.Collection {
	session, _ := bankTransaction.Client.StartSession()
	session.StartTransaction()

	return bankTransaction.Client.Database(config.DATABASE).Collection("banktransactions")
}

func (bankTransaction BankTransaction) Truncate(ctx context.Context) error {
	return bankTransaction.Collection().Drop(ctx)
}

func (bankTransaction BankTransaction) AddBankTransaction(ctx context.Context,
	bankAccountNo string, bankAccountOwner string,
	transactionAmount float64, reference string) (*BankTransaction, error) {

	bankTransaction.TransactionID = primitive.NewObjectID()
	bankTransaction.BankAccountNo = bankAccountNo
	bankTransaction.BankAccountOwner = bankAccountOwner
	bankTransaction.TransactionAmount = transactionAmount
	bankTransaction.Reference = reference
	bankTransaction.CreatedAt = time.Now().UTC()

	_, err := bankTransaction.Collection().InsertOne(ctx, bankTransaction)
	if err != nil {
		return nil, err
	}
	return &bankTransaction, nil

}

func (bankTransaction *BankTransaction) FindOne(ctx context.Context, transactionID string) error {

	oid, err := primitive.ObjectIDFromHex(transactionID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_transactionID", Value: oid},
	}

	result := bankTransaction.Collection().FindOne(ctx, filter)

	if result.Err() != nil {

		log.Println(result.Err())
		if result.Err().Error() == config.MONGO_NO_DOCUMENT {
			return errors.New("bank_transaction_not_found")
		}
		return result.Err()
	}

	err = result.Decode(&bankTransaction)
	if err != nil {
		return err
	}

	return nil

}

func (bankTransaction *BankTransaction) Update(ctx context.Context,
	transactionID string,
	bankAccountNo string, bankAccountOwner string,
	transactionAmount float64, reference string) error {

	oid, err := primitive.ObjectIDFromHex(transactionID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_transactionID", Value: oid},
	}

	set := bson.D{
		primitive.E{
			Key: "$set",
			Value: bson.D{
				primitive.E{Key: "bankAccountNo", Value: bankAccountNo},
				primitive.E{Key: "bankAccountOwner", Value: bankAccountOwner},
				primitive.E{Key: "transactionAmount", Value: transactionAmount},
				primitive.E{Key: "reference", Value: reference},
			},
		},
	}

	optionsAfter := options.After
	updateOptions := &options.FindOneAndUpdateOptions{
		ReturnDocument: &optionsAfter,
	}

	result := bankTransaction.Collection().FindOneAndUpdate(ctx, filter, set, updateOptions)
	if result.Err() != nil {
		return result.Err()
	}

	return result.Decode(&bankTransaction)

}

func (bankTransaction BankTransaction) Delete(ctx context.Context, transactionID string) error {

	oid, err := primitive.ObjectIDFromHex(transactionID)
	if err != nil {
		return err
	}

	filter := bson.D{
		primitive.E{Key: "_transactionID", Value: oid},
	}

	result, err := bankTransaction.Collection().DeleteOne(ctx, filter, nil)
	if err != nil {
		return err
	}
	log.Println(result)
	return nil

}

func (bankTransaction BankTransaction) ListBankTransactions(ctx context.Context) ([]BankTransaction, error) {

	bankTransactions := []BankTransaction{}
	filter := bson.D{}
	cursor, err := bankTransaction.Collection().Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	err = cursor.All(ctx, &bankTransactions)
	if err != nil {
		return nil, err
	}

	return bankTransactions, nil

}
