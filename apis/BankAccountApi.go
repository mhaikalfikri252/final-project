package apis

import (
	"encoding/json"
	"final-project/config"
	"final-project/connection"
	"final-project/datasql"
	"net/http"
)

type BankAccountApi struct {
	BaseApi

	datasql.BankAccount
}

func (bankAccountApi BankAccountApi) GetBankAccounts(w http.ResponseWriter, r *http.Request) {

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	baModel := datasql.CreateBankAccount(db)
	bankAccounts, err := baModel.GetBankAccounts()
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	} else {
		bankAccountApi.Json(w, bankAccounts, http.StatusOK)
		return
	}

}

func (bankAccountApi BankAccountApi) GetBankAccountByID(w http.ResponseWriter, r *http.Request) {

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	bankAccountID := bankAccountApi.QueryParam(r, "bankAccountNo")

	baModel := datasql.CreateBankAccount(db)
	bankAccount, err := baModel.FindOne(bankAccountID)
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	} else {
		bankAccountApi.Json(w, bankAccount, http.StatusOK)
		return
	}

}

func (bankAccountApi BankAccountApi) PostBankAccount(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bankAccountApi)
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	}

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	baModel := datasql.CreateBankAccount(db)

	err = baModel.Add(
		bankAccountApi.BankAccountNo,
		bankAccountApi.BankAccountOwner,
		bankAccountApi.Saldo)
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	} else {

		bankAccount, err := baModel.FindOne(bankAccountApi.BankAccountNo)
		if err != nil {
			bankAccountApi.Error(w, err)
			return
		} else {
			bankAccountApi.Json(w, bankAccount, http.StatusOK)
			return
		}
	}

}

func (bankAccountApi BankAccountApi) UpdateBankAccount(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bankAccountApi)
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	}

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	baModel := datasql.CreateBankAccount(db)

	err = baModel.Update(
		bankAccountApi.BankAccountNo,
		bankAccountApi.BankAccountOwner,
		bankAccountApi.Saldo)
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	} else {

		bankAccount, err := baModel.FindOne(bankAccountApi.BankAccountNo)
		if err != nil {
			bankAccountApi.Error(w, err)
			return
		} else {
			bankAccountApi.Json(w, bankAccount, http.StatusOK)
			return
		}
	}
}

func (bankAccountApi BankAccountApi) RemoveBankAccount(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&bankAccountApi)
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	}

	db := connection.OpenConnection(config.DEV)
	defer db.Close()

	baModel := datasql.CreateBankAccount(db)

	err = baModel.Remove(bankAccountApi.BankAccountNo)
	if err != nil {
		bankAccountApi.Error(w, err)
		return
	} else {
		bankAccountApi.Empty(w, http.StatusOK)
		return
	}

}
