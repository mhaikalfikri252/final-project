package datanosql

import (
	"context"
	"log"
	"testing"
	"final-project/connection"
	"final-project/helpers"
)

func TestBankTransaction_AddBankTransaction(t *testing.T) {

	ctx := context.TODO()

	conn, err := connection.OpenMongoDb(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer connection.CloseMongoDb(ctx, conn)

	bankTransactionNoSql := CreateBankTransaction(conn)
	err = bankTransactionNoSql.Truncate(ctx)
	if err != nil {
		t.Fatal(err)
	}

	bankAccountNo := "BCA"
	bankAccountOwner := "Mahmud"
	transactionAmount := float64(150000)
	reference := "Hutang"

	bankTransaction, err := bankTransactionNoSql.AddBankTransaction(ctx, 
		bankAccountNo, bankAccountOwner, 
		transactionAmount, reference)

	if err != nil {
		t.Fatal(err)
	}

	bankTransactions, err := bankTransactionNoSql.ListBankTransactions(ctx)
	if err != nil {
		t.Fatal(err)
	}
	log.Println("listBankTransactions=", helpers.ToJson(bankTransactions))

	err = bankTransactionNoSql.FindOne(ctx, bankTransaction.TransactionID.Hex())
	if err != nil {
		t.Fatal(err)
	}

	log.Println(helpers.ToJson(bankTransactionNoSql))

	// UPDATE
	bankAccountNo = "BNI"
	bankAccountOwner = "Marbub"
	transactionAmount = float64(10000)
	reference = "Sedekah"

	err = bankTransactionNoSql.Update(ctx, bankTransaction.TransactionID.Hex(),
		bankAccountNo, bankAccountOwner, transactionAmount, reference)

	if err != nil {
		t.Fatal(err)
	}
	log.Println(helpers.ToJson(bankTransactionNoSql))
	
	if bankTransactionNoSql.BankAccountNo != bankAccountNo {
		t.Fatal("Expected=", bankAccountNo, "actual=", bankTransactionNoSql.BankAccountNo)
	}
	if bankTransactionNoSql.BankAccountOwner != bankAccountOwner {
		t.Fatal("Expected=", bankAccountOwner, "actual=", bankTransactionNoSql.BankAccountOwner)
	}
	if bankTransactionNoSql.TransactionAmount != transactionAmount {
		t.Fatal("Expected=", transactionAmount, "actual=", bankTransactionNoSql.TransactionAmount)
	}
	if bankTransactionNoSql.Reference != reference {
		t.Fatal("Expected=", reference, "actual=", bankTransactionNoSql.Reference)
	}


	// DELETE BY ID
	// err = bankTransactionNoSql.Delete(ctx, bankTransactionNoSql.TransactionID.Hex())
	// if err != nil {
	// 	t.Fatal(err)
	// }

}
