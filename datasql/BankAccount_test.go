package datasql

import (
	"log"
	"testing"
	"final-project/config"
	"final-project/connection"
	"final-project/helpers"
)

func TestBankAccount_Add(t *testing.T) {

	db := connection.OpenConnection(config.CURRENT_PHASE)
	defer db.Close()

	bankAccountModel := CreateBankAccount(db)

	err := bankAccountModel.Truncate()
	if err != nil {
		t.Fatal(err.Error())
	}

	bankAccountNo := "BA-001"
	bankAccountOwner := "mhaikalfikri"
	saldo := float64(10000000)

	err = bankAccountModel.Add(bankAccountNo, bankAccountOwner, saldo)
	if err != nil {
		t.Fatal(err.Error())
	}

	bankAccounts, err := bankAccountModel.GetBankAccounts()
	if err != nil {
		t.Fatal(err)
	}
	log.Println("OnInserted", helpers.ToJson(bankAccounts))

	if len(bankAccounts) != 1 {
		t.Fatal("Expected=1, actual=", len(bankAccounts))
	}

	// update
	// bankAccountNo = "BA-002"
	// bankAccountOwner = "fulan"
	// saldo = float64(20000000)

	// err = bankAccountModel.Update(bankAccountNo, bankAccountOwner, saldo)
	// if err != nil {
	// 	t.Fatal(err.Error())
	// }

	// foundBankAccount, err := bankAccountModel.FindOne(bankAccountNo)
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// if foundBankAccount == nil {
	// 	t.Fatal("FindOne=BankAccount=", foundBankAccount)
	// }

	// if foundBankAccount.BankAccountNo != bankAccountNo {
	// 	t.Fatal("Expected=", bankAccountNo, "actual=", foundBankAccount.BankAccountNo)
	// }
	// if foundBankAccount.BankAccountOwner != bankAccountOwner {
	// 	t.Fatal("Expected=", bankAccountOwner, "actual=", foundBankAccount.BankAccountOwner)
	// }
	// if foundBankAccount.Saldo != saldo {
	// 	t.Fatal("Expected=", saldo, "actual=", foundBankAccount.Saldo)
	// }

	// bankAccounts, err = bankAccountModel.GetBankAccounts()
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// log.Println("OnUpdated", helpers.ToJson(bankAccounts))

	// Remove
	// err = bankAccountModel.Remove(bankAccountNo)
	// if err != nil {
	// 	t.Fatal(err.Error())
	// }

	// bankAccounts, err = bankAccountModel.GetBankAccounts()
	// if err != nil {
	// 	t.Fatal(err)
	// }

	// if len(bankAccounts) != 0 {
	// 	t.Fatal("Expected=0, actual=", len(bankAccounts))
	// }

}
