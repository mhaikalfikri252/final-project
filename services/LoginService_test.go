package services

import (
	"context"
	"final-project/config"
	"final-project/connection"
	"final-project/datanosql"
	"final-project/helpers"
	"log"
	"testing"
)

func TestLoginService_login(t *testing.T) {

	ctx := context.TODO()
	mongoClient, err := connection.OpenMongoDb(ctx)
	db := connection.OpenConnection(config.CURRENT_PHASE)
	if err != nil {
		t.Fatal(err)
	}
	defer connection.CloseMongoDb(ctx, mongoClient)
	defer db.Close()

	loginModel := datanosql.CreateLogin(mongoClient)
	err = loginModel.Truncate(ctx)
	if err != nil {
		t.Fatal(err)
	}

	// accountNo := "BA-002"
	// accountName := "Wakwaw"
	noHp := "081289899898"
	password := "wakwak123"
	// pin := 123456

	loginService := CreateLoginService(mongoClient, db)
	token, err := loginService.Login(ctx, noHp, password)

	if err != nil {
		t.Fatal(err)
	}

	log.Println("token=", token)

	login, err := loginService.ParseToken(ctx, token)
	if err != nil {
		t.Fatal(err)
	}

	log.Println("login=", helpers.ToJson(login))

}
